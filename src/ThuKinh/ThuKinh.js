import React, { useState } from 'react'
import { dataGlasses } from './dataGlasses'

export default function ThuKinh() {

    let [arrGlass, setArrGlass] = useState(dataGlasses);
    let handleThuKinh = (element) => {
        let glass = arrGlass.find((item)=>{
            return element.id == item.id
        })
        document.getElementById('glass').innerHTML = `
            <div class="glassChoice"><img src="${glass.url}" alt="" /></div>
            <div class="detailGlass">
                <p>Name: ${glass.name}</p>
                <p>Price: $${glass.price}</p>
                <p>${glass.desc}</p>
            </div>
        `
    }
    let renderGlassList = () =>{
        return arrGlass.map((item)=> {
            return(
                <div onClick={()=>{handleThuKinh(item)}} className='col col-4 imgGlass'><img src={item.url} alt="" /></div>
            )
        })
    }
    
  return (
    <div className='container'>
        <h2>Thử kính</h2>
        <div class="d-flex justify-content-between">
            <div className='row p-5 m-3'>
                {renderGlassList()}
            </div>
            <div>
                <div class="vglasses__model" id='glass'>
                    
                </div>
            </div>
        </div>
    </div>
    
  )
}
